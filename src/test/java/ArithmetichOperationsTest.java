import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jnfz on 05/09/2017.
 */
public class ArithmetichOperationsTest {
    @Test
    public void testAdd()
    {
        ArithmeticOperations operations = new ArithmeticOperations();
        Integer actual = operations.add(2, 6);
        Integer expected = 8;
        assertEquals(expected, actual);
    }

}
